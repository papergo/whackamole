import { createStore, applyMiddleware, compose } from 'redux';
import logger from 'redux-logger';
import { gameReducer } from '../reducers/gameReducer';

const store = createStore(
    gameReducer,
    compose(
        applyMiddleware(logger)
    )
    );

export default store;