import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    StyleSheet
} from 'react-native';
import Hole from "../components/hole";

import { connect } from 'react-redux';
import {changeHoleToUse, changeScore, reduceTime} from "../actions/action";

class GameScreen extends Component {
    state = {
        score: 0,
        gameTime: 30000,
        timeUp: false,
        timeRemaining: 30,
        lastHole: 0,
        hit: false,
        numberOfHoles: 12,
    };

    componentDidMount = () => {
        console.log(this.props);
        setInterval(()=>{
            if(this.props.gameState.timeRemaining>0) {
                this.props.reduceTime();
                // this.setState({timeRemaining: this.props.gameState.timeRemaining - 1});
            }
        },1000);
        setTimeout(()=>{
            this._showMole();
        },1000);
    };

    _popupTime = (min,max) => (
        Math.round(Math.random()*(max-min)+min)
    );

    _showMole = () => {
        let time = this._popupTime(300,1250);
        let holeToUse = Math.ceil(Math.random()*this.props.gameState.numberOfHoles);
        if (holeToUse === this.props.gameState.lastHole){
            holeToUse = Math.ceil(Math.random()*this.props.gameState.numberOfHoles);
        };
        this.props.changeHoleToUse(holeToUse);
        // this.setState({lastHole: holeToUse});
        setTimeout(()=>{
            this.props.changeHoleToUse(0);
            // this.setState({lastHole: 0});
            setTimeout(()=>{
                if(this.props.gameState.timeRemaining>0) {
                    this._showMole();
                }
            }, time)
        },time);
    };

    _hitHole = () => {
        this.props.changeHoleToUse(0);
        this.props.changeScore()
        // this.setState({score: this.props.gameState.score+1, lastHole: 0})
    };

    renderRowItem = (itemData) => {
        return(
            <Hole holeNumber={itemData.item} openHole={this.props.gameState.lastHole} hitHole={this._hitHole}/>
        )
    };


    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.title}>Whack-a-Mole Game!</Text>
                <Text>Time Remaining: {this.props.gameState.timeRemaining}</Text>
                {/*<Text>Hole: {this.props.gameState.lastHole}</Text>*/}
                <Text>Score: {this.props.gameState.score}</Text>
                {
                    this.props.gameState.timeRemaining === 0
                        ?
                        <Text>GAME FINISHED</Text>
                        :
                        null
                }
                <View style={styles.holesContainer}>
                    <FlatList
                        data={[1,2,3,4,5,6,7,8,9,10,11,12]}
                        numColumns={6}
                        scrollEnabled={false}
                        renderItem={this.renderRowItem}
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 20,
        textAlign: 'center',
        color: 'black',
        alignSelf: 'center',
        paddingTop: 100
    },
    holesContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    flatlist: {
        alignSelf: 'center'
    }
});

const mapStateToProps = (state) => ({
    gameState: state
});

const mapDispatchToProps = (dispatch) => {
    return{
        reduceTime: () => {
            dispatch(reduceTime())
        },
        changeHoleToUse: (payload) => {
            dispatch(changeHoleToUse(payload))
        },
        changeScore: () => {
            dispatch(changeScore())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GameScreen);