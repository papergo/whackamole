import * as ActionTypes from './actionTypes';

export function reduceTime(){
    return {
        type: ActionTypes.REDUCE_TIME
    }
};

export function changeHoleToUse(payload){
    return {
        type: ActionTypes.CHANGE_HOLE_TO_USE,
        payload
    }
};

export function changeScore(){
    return {
        type: ActionTypes.CHANGE_SCORE
    }
}