import {CHANGE_HOLE_TO_USE, CHANGE_SCORE, REDUCE_TIME} from "../actions/actionTypes";

const initialState = {
    score: 0,
    gameTime: 30000,
    timeUp: false,
    timeRemaining: 30,
    lastHole: 0,
    hit: false,
    numberOfHoles: 12,
};

export function gameReducer(state = initialState, action){
    switch (action.type){
        case REDUCE_TIME:
            return {
                ...state,
                timeRemaining: state.timeRemaining-1
            };
        case CHANGE_SCORE:
            return {
                ...state,
                score: state.score+1
            };
        case CHANGE_HOLE_TO_USE:
            return {
                ...state,
                lastHole: action.payload
            };
        default:
            return {
                ...state
            }
    }
}