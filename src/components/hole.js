import React, {Component} from 'react'
import {Image, StyleSheet, Text, TouchableWithoutFeedback, View} from 'react-native';

class Hole extends Component{
    state = {
        hit: false
    }

    render(){
        return(
            <TouchableWithoutFeedback
                onPress={()=>{
                    if(this.props.holeNumber === this.props.openHole) {
                        this.setState({hit: true});
                        setTimeout(()=>{
                            this.props.hitHole();
                            this.setState({hit:false})
                        },100);
                    }
                }
                }
            >
                <View>
                    {
                        this.state.hit ?
                            <Image
                                style={styles.image}
                                source={require('../images/hit.png')}
                            />
                            :
                            this.props.holeNumber === this.props.openHole ?
                                <Image
                                    style={styles.image}
                                    source={require('../images/open.png')}
                                />
                                :
                                <Image
                                    style={styles.image}
                                    source={require('../images/empty.png')}
                                />
                    }
                    {/*<Text style={styles.number}>{this.props.holeNumber}</Text>*/}
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

export default Hole;

const styles = StyleSheet.create({
    image: {
        width: 50,
        height: 50
    },
    number: {
        alignSelf: 'center'
    }
});